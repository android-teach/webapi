<?php

require_once 'func.php';

$apis = [
    [
        'name' => 'Register',
        'url' => url('user.php?action=register'),
        'method' => 'POST',
        'params' => [
            'username' => 'string - user name for register',
            'password' => 'string - password was md5 hash',
        ],
        'success' => 'registerSuccess.json',
        'fail' => 'registerFail.json',
    ],
    [
        'name' => 'Login',
        'url' => url('user.php?action=login'),
        'method' => 'POST',
        'params' => [
            'username' => 'string - user name for register',
            'password' => 'string - password was md5 hash',
        ],
        'success' => 'loginSuccess.json',
        'fail' => 'loginFail.json',
    ],
    [
        'name' => 'Get deal list',
        'url' => url('deal.php?action=get'),
        'method' => 'GET',
        'params' => [
            'token' => 'string - token of user',
            'month' => 'int - option - month of deal',
            'year' => 'int - option - year of deal (<strong>if filter, you must pass both month and year</strong>)',
        ],
        'success' => 'getDealSuccess.json',
        'fail' => 'getDealFail.json',
    ],
    [
        'name' => 'Add deal',
        'url' => url('deal.php?action=add'),
        'method' => 'POST',
        'params' => [
            'token' => 'string - token of user',
            'name' => 'string - name of deal',
            'note' => 'string - note of deal',
            'price' => 'double - price of deal',
            'group' => 'int - group number of deal',
            'date' => 'string - dd/mm/yyyy - Date of deal',
        ],
        'success' => 'addDealSuccess.json',
        'fail' => 'addDealFail.json',
    ],
    [
        'name' => 'Update deal',
        'url' => url('deal.php?action=update'),
        'method' => 'POST',
        'params' => [
            'id' => 'long - id of deal want update',
            'token' => 'string - token of user',
            'name' => 'string - name of deal',
            'note' => 'string - note of deal',
            'price' => 'double - price of deal',
            'group' => 'int - group number of deal',
            'date' => 'string - dd/mm/yyyy - Date of deal',
        ],
        'success' => 'addDealSuccess.json',
        'fail' => 'addDealFail.json',
    ],

    [
        'name' => 'Delete deal',
        'url' => url('deal.php?action=delete'),
        'method' => 'POST',
        'params' => [
            'id' => 'long - id of deal want delete',
            'token' => 'string - token of user',
        ],
        'success' => 'deleteDealSuccess.json',
        'fail' => 'addDealFail.json',
    ],
];

?>

<html>
<header>
    <title>API Doc For Developer</title>
</header>

<body>

<h2>API doc</h2>
<p>Postman doc: https://www.getpostman.com/collections/f0267a76c2e1920fa464</p>
<hr>

<?php foreach ($apis as $key => $api) : ?>
    <h3><?= $api['name'] ?></h3>
    <strong>Method:</strong> <?= $api['method'] ?> <br>
    <strong>Url:</strong> <?= $api['url'] ?> <br>
    <strong>Params:</strong><br>
    <?php foreach ($api['params'] as $k => $v) : ?>
        <li><strong><i><?= $k ?></i></strong>: <?= $v ?></li>
    <?php endforeach; ?>
    <strong>Success Result:</strong> <br>
    <pre><?php echo getResultFile($api['success']) ?></pre>
    <strong>Fail Result:</strong><br>
    <pre><?php echo getResultFile($api['fail']) ?></pre>

    <hr>

<?php endforeach; ?>

</body>

</html>
