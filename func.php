<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/2/18
 * Time: 16:15
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

define("DOMAIN", 'http://teach.cachhoc.net');
define("BASE_URL", DOMAIN . '/ictu/android');

function pushResult($data, $msg, $isSuccess)
{
    header('Content-Type: application/json');
    $rs = [
        'status' => $isSuccess,
        'msg' => $msg,
        'data' => $data,
    ];
    echo json_encode($rs, JSON_PRETTY_PRINT);
}

function getResultFile($file) {
    return file_get_contents("store/apiresult/$file");
}

function url($url)
{
    return BASE_URL . '/' . $url;
}

function echoUrl($url)
{
    echo url($url);
}

function isDate($string) {
    $matches = array();
    $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
    if (!preg_match($pattern, $string, $matches)) return false;
    if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
    return true;
}