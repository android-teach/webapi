<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/2/18
 * Time: 16:15
 */

require_once "func.php";

function getUserFilePath()
{
    return "store/users/users.json";
}

function register($params)
{
    $users = file_get_contents(getUserFilePath());
    $users = json_decode($users, true);

    $username = trim($params['username']);
    $password = trim($params['password']);

    $isRegister = false;
    foreach ($users as $u) {
        if ($u['username'] == $username) {
            $isRegister = true;
            break;
        }
    }

    if (empty($username))
        pushResult(null, "username empty", false);
    else if ($isRegister) {
        pushResult(null, "username was exits", false);
    } else {
        $time = time();
        $token = md5($time);
        $user = [
            'id' => $time,
            'username' => $username,
            'password' => $password,
            'token' => $token,
        ];
        $users[] = $user;
        file_put_contents(getUserFilePath(), json_encode($users));

        require_once "deal.php";
        file_put_contents(getDealFilePath($token), json_encode([]));

        unset($user['password']);
        pushResult($user, "success", true);
    }
}

function login($params)
{
    $users = file_get_contents(getUserFilePath());
    $users = json_decode($users, true);

    $username = trim($params['username']);
    $password = trim($params['password']);

    $user = null;
    foreach ($users as $u) {
        if ($u['username'] == $username && $password == $u['password']) {
            $user = $u;
            break;
        }
    }

    if ($user == null) {
        pushResult(null, 'incorrect username or password', false);
    } else {
        unset($user['password']);
        pushResult($user, 'success', true);
    }
}

$action = $_REQUEST['action'];

if ($action == 'register') {
    register($_POST);
} else if ($action == 'login') {
    login($_POST);
}