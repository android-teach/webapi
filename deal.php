<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/2/18
 * Time: 15:30
 */

require_once "func.php";

function getDealFilePath($token)
{
    return "store/deals/$token.json";
}

function getDealList($token)
{
    if (file_exists(getDealFilePath($token))) {
        $data = file_get_contents(getDealFilePath($token));
        $data = json_decode($data, true);
        return $data;
    } else {
        return "no";
    }

}

function addDeal($token, $params)
{
    if (isset($params['date']) && isDate($params['date'])) {
        $time = time();
        $date = str_replace('/', '-', $params['date']);
        $date = date('d/m/Y', strtotime($date));
        $deal = [
            'name' => $params['name'],
            'note' => $params['note'],
            'price' => $params['price'],
            'group' => $params['group'],
            'date' => $date,
        ];
        $data = getDealList($token);
        if (!isset($params['id'])) {
            $deal['id'] = $time;
            $data[] = $deal;
            pushResult($deal, 'success', true);
        } else {
            $isFound = false;
            foreach ($data as &$d) {
                if ($params['id'] == $d['id']) {
                    $deal['id'] = $d['id'];
                    $d = $deal;
                    $isFound = true;
                    break;
                }
            }
            if (!$isFound) {
                pushResult(null, 'not found iteam', false);
            } else {
                pushResult($deal, 'success', true);
            }
        }
        file_put_contents(getDealFilePath($token), json_encode($data));
    } else {
        pushResult(null, 'incorrect date', false);
    }
}

function deleteDeal($token, $params)
{
    $data = getDealList($token);
    if (isset($params['id'])) {
        $isFound = false;
        foreach ($data as $k => &$d) {
            if ($params['id'] == $d['id']) {
                unset($data[$k]);
                $isFound = true;
                break;
            }
        }
        if (!$isFound) {
            pushResult(null, 'not found iteam', false);
        } else {
            pushResult(null, 'success', true);
            file_put_contents(getDealFilePath($token), json_encode($data));
        }
    }
}

$action = $_REQUEST['action'];

if ($action == 'get') {
    $params = $_GET;
    $data = getDealList($params['token']);
    if ($data == "no") {
        pushResult(null, "user not exits", false);
    } else {

        if (isset($params['month']) && isset($params['year'])) {
            $rs = array();
            foreach ($data as $deal) {
                $date = str_replace('/', '-', $deal['date']);
                $month = date('m', strtotime($date));
                $year = date('Y', strtotime($date));
                if ($params['month'] == $month && $params['year'] == $year) {
                    $rs[] = $deal;
                }
            }
        } else {
            $rs = $data;
        }
        pushResult($rs, "success", true);
    }
}
if ($action == 'add') {
    $params = $_POST;
    $token = $params['token'];

    if ($token == 'example') {
        pushResult(null, "can not add to example user.", false);
    } else if (!file_exists(getDealFilePath($token))) {
        pushResult(null, "user not exits", false);
    } else {
        addDeal($token, $params);
    }
}

if ($action == 'update') {
    $params = $_POST;
    $token = $params['token'];

    if ($token == 'example') {
        pushResult(null, "can not update to example user.", false);
    } else if (!file_exists(getDealFilePath($token))) {
        pushResult(null, "user not exits", false);
    } else {
        addDeal($token, $params);
    }
}


if ($action == 'delete') {
    $params = $_POST;
    $token = $params['token'];

    if ($token == 'example') {
        pushResult(null, "can not delte to example user.", false);
    } else if (!file_exists(getDealFilePath($token))) {
        pushResult(null, "user not exits", false);
    } else {
        deleteDeal($token, $params);
    }
}